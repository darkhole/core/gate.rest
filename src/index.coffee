# core libs
Gate        = require '@dark-hole/gate'
Transaction = require '@dark-hole/gate/dist/lib/_transaction'

# packages
express     = require 'express'
bodyParser  = require 'body-parser'
http        = require 'http'

class GateRest

  constructor : ( srv, opts ) ->

    @_transactions = []

    @_app    = express()
    @_server = http.createServer @_app
    @_gate   = new Gate @_server, opts

    @_app.use bodyParser.json()
    @_app.use @router()

    @protocol()

    @_server.listen srv

  # protocol
  protocol : ->
    it = @

    @_gate.on 'connection', ( socket ) ->

      # hook to use rest response
      socket.use ( packet, next ) ->
        return next() if packet[0] not in [ 'end:success', 'end:error' ]

        data        = packet[1]
        transaction = it.getGate()._stack.get data

        return next() if transaction?.__forward

        packet[1]?[ 'rest' ] = true
        next()

      # send error response
      socket.on 'end:error', ( data ) ->
        item = ( it._transactions.filter ( el ) -> el.__id is data.__id ).shift()
        if item
          index = it._transactions.indexOf item
          it._transactions.splice index, 1
          item.__res.send data.err

      # send success response
      socket.on 'end:success', ( data ) ->
        if data.rest
          item = ( it._transactions.filter ( el ) -> el.__id is data.__id ).shift()
          if item
            index = it._transactions.indexOf item
            it._transactions.splice index, 1
            item.__res.send data.result

  router : ->
    it = @
    ( req, res, next ) ->
      return next() if req.method is 'OPTIONS'

      if it._gate._passport
        pack =
          __name    : it._gate._passport
          __forward : req.path.replace( /^\//, '' ).replace( /\/$/, '' ).split( /\// ).join ':'
          __body    : Object.assign req.body, req.query
      else
        pack =
          __name : req.path.replace( /^\//, '' ).replace( /\/$/, '' ).split( /\// ).join ':'
          __body : Object.assign req.body, req.query

      transaction = new Transaction pack, null, 'rest'

      it._transactions.push
        __id  : transaction.__id
        __req : req
        __res : res

      if not it.getGate()._trusted.validService( transaction.getName() )
        return res.status( 404 ).send 'invalid service'

      it.getGate().process transaction

  getApp  : -> @_app
  getGate : -> @_gate

module.exports = GateRest

